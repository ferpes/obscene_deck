#include <UTFT.h>
#include <URTouch.h>
#include <Keyboard.h>

UTFT    myGLCD(CTE32_R2,38,39,40,41);
URTouch  myTouch( 6, 5, 4, 3, 2);

// Declare which fonts we will be using
// big font chars are 16x16
extern uint8_t BigFont[];

// 320px x 240x
// btn's are 90x60
int x,y;
int activeBtn = 0;
int btnX[] = {15, 115, 215, 15, 115, 215, 15, 115, 215,};
int btnY[] = {20, 20, 20, 90, 90, 90, 160, 160, 160};

// The the value of each index in btnLabel should match the value of the same index in btnVal
// ex: when the "Main" btn is pressed, we take it's index (0) and match it in btnVal[0], so the arduino sends "KEY_F13"
String btnLabel[] = {"Main","Over","Soldr","Laser","Mill","Routr","BRB","End","Empty"};
word btnVal[] = {"KEY_F13","KEY_F14","KEY_F15","KEY_F16","KEY_F17","KEY_F18","KEY_F19","KEY_F20","KEY_F21"};

/*************************
**   Custom functions   **
*************************/

void drawButton(int btn, word color, word backColor, bool active=false)
{
  // clear the button area
  myGLCD.setColor(VGA_BLACK);
  myGLCD.setBackColor(VGA_BLACK);
  myGLCD.fillRoundRect(btnX[btn],btnY[btn], btnX[btn]+90, btnY[btn]+60);
  
  if (active==true)
  {
    myGLCD.setColor(backColor);
    myGLCD.fillRoundRect(btnX[btn],btnY[btn], btnX[btn]+90, btnY[btn]+60);
    myGLCD.setColor(color);
    myGLCD.setBackColor(backColor);
  }
  else 
  {
    myGLCD.setColor(color);
    myGLCD.drawRoundRect(btnX[btn],btnY[btn], btnX[btn]+90, btnY[btn]+60);
  }

  myGLCD.print(btnLabel[btn], btnX[btn]+45-(btnLabel[btn].length()*8), btnY[btn]+30-8);
  myGLCD.setBackColor(VGA_BLACK);
}

void sendDebugMsg(String msg)
{
  myGLCD.setColor (VGA_BLACK);
  myGLCD.print("                ", CENTER, 2);
  myGLCD.setColor(255, 255, 255);
  myGLCD.print(msg, CENTER, 2);
}


/*************************
**  Required functions  **
*************************/

void setup()
{

  myGLCD.InitLCD();
  myGLCD.clrScr();

  myTouch.InitTouch();
  myTouch.setPrecision(PREC_MEDIUM);

  myGLCD.setFont(BigFont);
  myGLCD.setBackColor(0, 0, 255);

  for (int i=0; i < 9; i++) {
    drawButton(i,VGA_LIME,VGA_BLACK);
  }

  Keyboard.begin();
}

void loop()
{
  if (myTouch.dataAvailable()){
    
    myTouch.read();
    x=myTouch.getX();
    y=myTouch.getY();

    for (int i = 0; i < 9; i++){
      
      if (y >= btnY[i] && y <= btnY[i]+60){
        
        for (int j = i; j <= i+2; j++){
          
          if (x >= btnX[j] && x <= btnX[j]+90){
            
            int btn = i+(j-i);
            
            //sendDebugMsg(String(x)+":"+String(y)+" btn:"+String(btn));
            
            while (myTouch.dataAvailable())
              myTouch.read();

            // Deactivate active button
            drawButton(activeBtn, VGA_LIME, VGA_BLACK);
              
            // Set button background to green and text black
            drawButton(btn, VGA_BLACK, VGA_LIME, true);

            activeBtn = btn;

            // send button (i+(j-i)) value to computer
            Keyboard.press(KEY_F13);
            delay(100);
            Keyboard.release(KEY_F13);
            break;
          }
        }
        break;        
      }
    }
  }
}