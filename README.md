# obscene_deck
![Image of the diy stream deck](obscene_deck.jpg)

# Objectives
This project is to make an accessible, DIY, OBS scene switching deck with cheap hobby components.
It uses an arduino leonardo and a tft touchscreen to create the buttons.
The keyboard presses and the button values are customizable in the firmware.

# BOM
* 1x Arduino Leonardo
* 1x TFT Touchscreen Hat for Arduino Uno
* 4x M3x8
* 4x M4x10 - M4X20 (there's room for varying lengths, just use whatever you have on hand)
* 1x Micro USB cable with both power and data lines

# Pregenerated STL's
If you want to purchase pre-generated STL's from the fusion project so you don't need to do it yourself, they are available on my [cults3d page](https://cults3d.com/en/3d-model/gadget/obscene-deck)

